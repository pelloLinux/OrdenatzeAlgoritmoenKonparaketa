package fitxategiakSortu;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class fitxSortu {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			
			System.out.println("Fitxategiak Sortzen");
			fitxategiEzOrdenatuaSortu(10, "fitxategiak/ezOrdenatua10.txt");
			fitxategiEzOrdenatuaSortu(100, "fitxategiak/ezOrdenatua100.txt");
			fitxategiEzOrdenatuaSortu(1000, "fitxategiak/ezOrdenatua1000.txt");
			fitxategiEzOrdenatuaSortu(10000, "fitxategiak/ezOrdenatua10000.txt");
			fitxategiEzOrdenatuaSortu(100000, "fitxategiak/ezOrdenatua100000.txt");
			fitxategiEzOrdenatuaSortu(1000000, "fitxategiak/ezOrdenatua1000000.txt");
			fitxategiOrdenatuaSortuGora(10, "fitxategiak/ordenatuaGora10.txt");
			fitxategiOrdenatuaSortuGora(100, "fitxategiak/ordenatuaGora100.txt");
			fitxategiOrdenatuaSortuGora(1000, "fitxategiak/ordenatuaGora1000.txt");
			fitxategiOrdenatuaSortuGora(10000, "fitxategiak/ordenatuaGora10000.txt");
			fitxategiOrdenatuaSortuGora(100000, "fitxategiak/ordenatuaGora100000.txt");
			fitxategiOrdenatuaSortuGora(1000000, "fitxategiak/ordenatuaGora1000000.txt");
			fitxategiOrdenatuaSortuBehera(10, "fitxategiak/ordenatuaBehera10.txt");
			fitxategiOrdenatuaSortuBehera(100, "fitxategiak/ordenatuaBehera100.txt");
			fitxategiOrdenatuaSortuBehera(1000, "fitxategiak/ordenatuaBehera1000.txt");
			fitxategiOrdenatuaSortuBehera(10000, "fitxategiak/ordenatuaBehera10000.txt");
			fitxategiOrdenatuaSortuBehera(100000, "fitxategiak/ordenatuaBehera100000.txt");
			fitxategiOrdenatuaSortuBehera(1000000, "fitxategiak/ordenatuaBehera1000000.txt");
			fitxategiEzOrdenatuaSortu2(10, "fitxategiak/2ezOrdenatua10.txt");
			fitxategiEzOrdenatuaSortu2(100, "fitxategiak/2ezOrdenatua100.txt");
			fitxategiEzOrdenatuaSortu2(1000, "fitxategiak/2ezOrdenatua1000.txt");
			fitxategiEzOrdenatuaSortu2(10000, "fitxategiak/2ezOrdenatua10000.txt");
			fitxategiEzOrdenatuaSortu2(100000, "fitxategiak/2ezOrdenatua100000.txt");
			fitxategiEzOrdenatuaSortu2(1000000, "fitxategiak/2ezOrdenatua1000000.txt");
			System.out.println("AMAITUTA");
						
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void fitxategiOrdenatuaSortuGora(int n, String fitxIzena) throws FileNotFoundException{

		PrintWriter fitx = new PrintWriter(fitxIzena);
		for(int i=1; i<=n; i++)
			fitx.println(i);
		fitx.close();
	}
	public static void fitxategiOrdenatuaSortuBehera(int n, String fitxIzena) throws FileNotFoundException{

		PrintWriter fitx = new PrintWriter(fitxIzena);
		for(int i=n; i>=1; i--)
			fitx.println(i);
		fitx.close();
	}

	public static void fitxategiEzOrdenatuaSortu(int n, String fitxIzena) throws FileNotFoundException{

		numeroAleatorio random = new numeroAleatorio(1, n);//hemen ez du zertan horrela izan, zenbaki kopurua baino handiagoa zergatik ez?
		PrintWriter fitx = new PrintWriter(fitxIzena);
		for(int i=0; i<n; i++)
			fitx.println(random.getRandom());
		fitx.close();
		
	}
	public static void fitxategiEzOrdenatuaSortu2(int n, String fitxIzena) throws FileNotFoundException{

		numeroAleatorio random = new numeroAleatorio(1, 10000000);//hemen ez du zertan horrela izan, zenbaki kopurua baino handiagoa zergatik ez?
		PrintWriter fitx = new PrintWriter(fitxIzena);
		for(int i=0; i<n; i++)
			fitx.println(random.getRandom());
		fitx.close();
		
	}


}
