package OrdenatzeAlgoritmoak;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class nagusia {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		long startTime;
		long endTime;
		int[] array;
		
		
		Scanner s = new Scanner(System.in);
		System.out.println("Zenbat numeroko sekuentzia?(10,100,1000,10000,100000,1000000)");
		int n = s.nextInt();
		System.out.println("Sekuentziako zenbat elementu txikienak?");
		int m = s.nextInt();

		ordenatzeAlgoritmoak algoritmoak = new ordenatzeAlgoritmoak();	
		
		System.out.println("********************************");
		System.out.println("         ORDENATU GABE");
		System.out.println("********************************");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ezOrdenatua"+n+".txt", n);
		System.out.println("Selection Sort");//Aukeraketa
		startTime = System.currentTimeMillis();
		algoritmoak.selectionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Selection Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Insertion Sort");//txertaketa
		startTime = System.currentTimeMillis();
		algoritmoak.insertionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Insertion Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Merge Sort");
		startTime = System.currentTimeMillis();
		MergeSort2 merge = new MergeSort2();
		merge.sort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Quick Sort");
		startTime = System.currentTimeMillis();
		algoritmoak.quickSort(0, array.length-1, array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Metak erabiliz");
		startTime = System.currentTimeMillis();
		kTxikienakLortu algoritmoak2 = new kTxikienakLortu();
		algoritmoak2.metakErabiliz(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Metak Erabiliz: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		System.out.println("********************************");
		System.out.println("         ORDENATU GABE 2");
		System.out.println("********************************");
		System.out.println();
		
		array = arrayLortu("fitxategiak/2ezOrdenatua"+n+".txt", n);
		System.out.println("Selection Sort");//Aukeraketa
		startTime = System.currentTimeMillis();
		algoritmoak.selectionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Selection Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/2ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Insertion Sort");//txertaketa
		startTime = System.currentTimeMillis();
		algoritmoak.insertionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Insertion Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/2ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Merge Sort");
		startTime = System.currentTimeMillis();
		merge = new MergeSort2();
		merge.sort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/2ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Quick Sort");
		startTime = System.currentTimeMillis();
		algoritmoak.quickSort(0, array.length-1, array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/2ezOrdenatua"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Metak erabiliz");
		startTime = System.currentTimeMillis();
		algoritmoak2 = new kTxikienakLortu();
		algoritmoak2.metakErabiliz(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Metak Erabiliz: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		System.out.println("********************************");
		System.out.println("         GORANTZ ORDENATUTA");
		System.out.println("********************************");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ordenatuaGora"+n+".txt", n);
		System.out.println("Selection Sort");//Aukeraketa
		startTime = System.currentTimeMillis();
		algoritmoak.selectionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Selection Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/ordenatuaGora"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Insertion Sort");//txertaketa
		startTime = System.currentTimeMillis();
		algoritmoak.insertionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Insertion Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/ordenatuaGora"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Merge Sort");
		startTime = System.currentTimeMillis();
		merge = new MergeSort2();
		merge.sort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ordenatuaGora"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Quick Sort");
		startTime = System.currentTimeMillis();
		algoritmoak.quickSort(0, array.length-1, array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ordenatuaGora"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Metak erabiliz");
		startTime = System.currentTimeMillis();
		algoritmoak2 = new kTxikienakLortu();
		algoritmoak2.metakErabiliz(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Metak Erabiliz: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		System.out.println("********************************");
		System.out.println("         BERANTZ ORDENATUTA");
		System.out.println("********************************");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ordenatuaBehera"+n+".txt", n);
		System.out.println("Selection Sort");//Aukeraketa
		startTime = System.currentTimeMillis();
		algoritmoak.selectionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Selection Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/ordenatuaBehera"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Insertion Sort");//txertaketa
		startTime = System.currentTimeMillis();
		algoritmoak.insertionSort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Insertion Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		array = arrayLortu("fitxategiak/ordenatuaBehera"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Merge Sort");
		startTime = System.currentTimeMillis();
		merge = new MergeSort2();
		merge.sort(array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();
		
		array = arrayLortu("fitxategiak/ordenatuaBehera"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Quick Sort");
		startTime = System.currentTimeMillis();
		algoritmoak.quickSort(0, array.length-1, array);
		arraykoTxikienakInpr(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Merge Sort: " + (endTime - startTime) + " milisegundu");
		System.out.println();

		array = arrayLortu("fitxategiak/ordenatuaBehera"+n+".txt", n);//berriro array desordenatua hartu behar da
		System.out.println("Metak erabiliz");
		startTime = System.currentTimeMillis();
		algoritmoak2 = new kTxikienakLortu();
		algoritmoak2.metakErabiliz(array, m);
		endTime = System.currentTimeMillis();
		System.out.println("Metak Erabiliz: " + (endTime - startTime) + " milisegundu");
		System.out.println();


		s.close();
		//System.out.println(Arrays.toString(array));

	}

	private static int[] arrayLortu(String fitxIz, int n) throws FileNotFoundException {

		int[] array = new int[n];
		File fitx = new File(fitxIz);
		Scanner in = new Scanner(fitx);
		int i=0;
		while(in.hasNextLine() && i<n){
			array[i]=in.nextInt();
			i++;
		}
		in.close();
		return array;
	}

	public static void arraykoTxikienakInpr(int[] array, int k){

		System.out.print("[");
		for (int i = 0; i < k; i++) {
			System.out.print(array[i]+",");
		}
		System.out.println("]");
	}

}