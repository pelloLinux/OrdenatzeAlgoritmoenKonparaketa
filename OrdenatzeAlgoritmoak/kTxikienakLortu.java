package OrdenatzeAlgoritmoak;

public class kTxikienakLortu {

	public void metakErabiliz(int[] array, int k){

		Heap meta = new Heap(k);
		for (int i = 0; i < k; i++)
			meta.insert(array[i]);

		for(int i=k; i<array.length; i++)
		{
			if(meta.getIndexValue(0)>array[i])
				meta.change(0, array[i]);
		}
		meta.displayHeap();
	}

}
